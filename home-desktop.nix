{ lib, scripts, pkgs, pkgs-unstable, ... }:
{
  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.allowUnfreePredicate = _: true;

  imports = [
    ./modules/gpg.nix
    ./modules/hyprland.nix
    ./modules/sway.nix
    ./modules/waybar.nix
    ./modules/alacritty.nix
    ./modules/qutebrowser.nix
  ];

  # env vars
  home.sessionVariables = {
    NIXOS_OZONE_WL = "1";
    QT_QPA_PLATFORM = "wayland;xcb";
    QT_QPA_PLATFORMTHEME = "qt5ct";
  };

  # theming
  qt = {
    enable = true;
    platformTheme.name = "qtct";
  };
  gtk = {
    enable = true;
    cursorTheme = {
      name = "Bibata-Modern-Ice";
      package = pkgs.bibata-cursors;
    };
    theme = {
      name = "adw-gtk3";
      package = pkgs.adw-gtk3;
    };
    #iconTheme = {
    #  name = "GruvboxPlus";
    #  package = pkgs.gruvboxPlus;
    #};
  };

  # packages
  home.packages = lib.mkMerge [
    (with pkgs; [
      # GUI programs
      firefox
      chromium
      imv
      mpv
      grimblast
      libreoffice
      evince

      # other
      ffmpeg
      amdgpu_top

      vesktop

      uiua

      # de stuff
      rofi
      wofi
      wl-clipboard
      dotool
      (aspellWithDicts (d: [d.en]))

      dolphin
      libsForQt5.qt5ct                 # Qt5 Configuration Tool
      adwaita-qt                         # Qt5 themes
      adwaita-qt6

      # games
      runelite
    ])
    (with pkgs-unstable; [
      #logseq
      nil
    ])
  ];

  xdg = {
    desktopEntries = {
      qutesession = {
        name = "Qutebrowser Session";
        genericName = "Web Browser";
        exec = lib.getExe scripts.qutebrowser-current-session;
        terminal = false;
        categories = [ "Application" "Network" "WebBrowser" ];
        mimeType = [ "text/html" ];
      };
    };
    mimeApps = {
      enable = true;
      
      defaultApplications = {
        "text/html" = "qutesession.desktop";
        "x-scheme-handler/http" = "qutesession.desktop";
        "x-scheme-handler/https" = "qutesession.desktop";
        "x-scheme-handler/about" = "qutesession.desktop";
        "x-scheme-handler/unknown" = "qutesession.desktop";
      };
    };
  };

  programs.obs-studio = {
    enable = true;
    plugins = with pkgs.obs-studio-plugins; [
      obs-vkcapture
      obs-pipewire-audio-capture
    ];
  };

  programs.vscode = {
    enable = true;
    mutableExtensionsDir = false;
    package = pkgs-unstable.vscode.fhsWithPackages 
      (ps: with ps; [
        rustup
        zlib
        openssl
        openssl.dev
        pkg-config
        gcc
        direnv
        uiua
      ]
    );
    extensions = with pkgs-unstable.vscode-extensions; [
      arrterian.nix-env-selector
      
      jnoortheen.nix-ide
      arrterian.nix-env-selector
      mkhl.direnv
      esbenp.prettier-vscode

      rust-lang.rust-analyzer
      fill-labs.dependi
      tamasfe.even-better-toml
      vadimcn.vscode-lldb
      uiua-lang.uiua-vscode
      ms-python.python
      ms-python.vscode-pylance
      ms-python.isort
      usernamehw.errorlens
      continue.continue

      ms-vsliveshare.vsliveshare
    ] ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
      {
        name = "nftables";
        publisher = "ombratteng";
        version = "0.7.0";
        sha256 = "sha256-nxs1C3MA+9dQylJs9RLQJ35SRZNanIWeYAaeVVzs2Fo=";
      }
      {
        name = "sqltools";
        publisher = "mtxr";
        version = "0.28.3";
        sha256 = "sha256-bTrHAhj8uwzRIImziKsOizZf8+k3t+VrkOeZrFx7SH8=";
      }
      {
        name = "sqltools-driver-mssql";
        publisher = "mtxr";
        version = "0.4.3";
        sha256 = "sha256-j8QqdJTumBfeaoPIpy78Vzg2mI6kazUBraJyKR0wDMY=";
      }
      {
        name = "sqltools-driver-pg";
        publisher = "mtxr";
        version = "0.5.4";
        sha256 = "sha256-XnPTMFNgMGT2tJe8WlmhMB3DluvMZx9Ee2w7xMCzLYM=";
      }
    ];
    userSettings = {
      # tab size
      "editor.tabSize" = 2;
      "prettier.tabWidth" = 2;
      "python.formatting.autopep8Args" = [
        "--indent-size=2"
        "--ignore E121"
      ];

      # disable the excessive header
      "window.titleBarStyle" = "native";
      "window.customTitleBarVisibility" = "never";
      "editor.titleBar.enabled" = false;
      "window.commandCenter" = false;
      "workbench.layoutControl.enabled" = false;
      "window.menuBarVisibility" = "hidden";

      # misc editor
      "editor.fontFamily" = "\"Fira Code\",\"Hack Nerd Font\"";
      "editor.fontLigatures" = true;
      "editor.wordWrap" = "on";
      "editor.formatOnPaste" = true;
      "editor.formatOnSave" = true;
      "editor.codeActionsOnSave" = {
        "source.fixAll" = "never";
        "source.fixAll.eslint" = "explicit";
      };
      "editor.defaultFormatter" = "esbenp.prettier-vscode";

      # disable updates
      "update.mode" = "none";
      "extensions.autoUpdate" = false;

      # misc
      "security.workspace.trust.enabled" = false;
      "debug.allowBreakpointsEverywhere" = true;

      # extensions
      ## liveshare
      "liveshare.accessibility.voiceEnabled" = false;

      ## nix-ide
      "nix.enableLanguageServer" = true;
      "nix.serverPath" = "${lib.getExe pkgs-unstable.nil}";

      ## nix env selector
      "nixEnvSelector.suggestion" = false;

      ## rust-analyzer
      "rust-analyzer.check.command" = "clippy";

      ## nftables
      "[nft]" = {
        "editor.formatOnSave" = true;
        "editor.defaultFormatter" = "omBratteng.nftables";
      };

      ## uiua
      "uiua.executablePath" = "${pkgs-unstable.uiua}/bin/uiua";
    };
  };

  dotfiles.age.home-assistant.enable = true;

  services.mako = {
    enable = true;
  };

  services.syncthing = {
    enable = true;
  };

  #services.cliphist.enable = true; # currently saves passwords. Wait for a fix

  modules.alacritty.enable = true;
  modules.gpg.enable = true;
  modules.hyprland = {
    enable = true;
    monitors = [
      "monitor=DP-1,1920x1080@144,auto,1"
      "monitor=DP-3,1920x1080@144,auto,1"
      "monitor=HDMI-A-1,1920x1080@60,auto,1"
    ];
  };
  #modules.sway.enable = true;
  #modules.wayfire.enable = false;
  modules.waybar.enable = true;
  modules.qutebrowser.enable = true;
}
