{
  description = "@Lindenk's dotfiles using nix and home-manager";

  inputs = {
    # 24.11
    nixpkgs.url = "github:nixos/nixpkgs/1546c45c538633ae40b93e2d14e0bb6fd8f13347";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/95eacfb6d1accf8d59a9e570d26d04ad693d2ec3";
    home-manager = {
      # 24.11
      url = "github:nix-community/home-manager/9d3d080aec2a35e05a15cedd281c2384767c2cfe";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    agenix.url = "github:ryantm/agenix";
  };

  outputs = { nixpkgs, nixpkgs-unstable, home-manager, agenix, ... }:
    {
      packages = builtins.listToAttrs (builtins.map (system:
        let
          pkgs = import nixpkgs { inherit system; };
          pkgs-unstable = import nixpkgs-unstable { inherit system; config.allowUnfree = true; };
          scripts = import ./scripts.nix { inherit pkgs; };

          mkHome = { modules }:
            home-manager.lib.homeManagerConfiguration {
              inherit pkgs;

              modules = [
                ./home.nix
                ./secrets/default.nix
                agenix.homeManagerModules.age
                {
                  home.packages = [
                    agenix.packages.${system}.default
                  ];
                }
              ] ++ modules;
              extraSpecialArgs = { inherit pkgs-unstable nixpkgs scripts; };
            };
        in
        {
          name = system;
          value.homeConfigurations = {
            lindenk-desktop = mkHome { modules = [ ./home-desktop.nix ]; };
            lindenk-server = mkHome { modules = [ ./home-server.nix ]; };
            lindenk-minimal = home-manager.lib.homeManagerConfiguration {
              inherit pkgs;

              modules = [
                ./home-minimal.nix
              ];
              extraSpecialArgs = { inherit nixpkgs; };
            };
          };
        }) [ "x86_64-linux" "aarch64-linux" "i686-linux" ]);

      updater = ./updater.nix;
    };
}
