{ lib, config, ... }:
with lib;
let
  cfg = config.dotfiles.age;
in
{
  options.dotfiles.age = {
    home-assistant.enable = mkEnableOption "Enable secrets for home-assistant";
  };

  config.age.secrets = mkMerge [
    (mkIf cfg.home-assistant.enable {
      "home-assistant".file = ./home-assistant.age;
    })
  ];
}
