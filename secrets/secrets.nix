let
  lindenk = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPd3zNd5Sex04cFVP6VXisTSQmH1nKI4m5vYKrz/mZci";
  users = [ lindenk ];
in
{
  "home-assistant.age".publicKeys = users;
}
