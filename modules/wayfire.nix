{ pkgs, config, lib, ... }:
with lib;
let cfg = config.modules.sway;
in
{
  options.modules.sway.enable = mkEnableOption "Wayland WM with effects";

  config = mkIf cfg.enable {
    
  };
}
