{ pkgs, lib, config, ... }:
with lib;
let
  cfg = config.modules.zsh;
in
{
  options.modules.zsh.enable = mkEnableOption "zsh";

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      zsh
      mcfly
      zoxide
      fzf
      skim
    ];

    # cool test, doesnt do what I want
    #programs.direnv = {
    #  enable = true;
    #  enableZshIntegration = true;
    #  silent = true;
    #  nix-direnv.enable = true;
    #};

    programs.starship = {
      enable = true;

      settings = {
        username = {
          style_user = "bold green";
          format = "[$user]($style)";
        };


        hostname = {
          ssh_only = true;
          format = "[$ssh_symbol](bold red)[$hostname](bold blue) ";
          ssh_symbol = "@";
        };

        nix_shell = {
          symbol = "❄️ ";
          format = "[$symbol$name]($style)";
        };
      };
    };


    programs.zsh = {
      enable = true;

      dotDir = ".config/zsh";

      enableCompletion = true;
      autosuggestion.enable = true;
      syntaxHighlighting.enable = true;

      initExtra = ''
        setopt auto_cd

        # ls on cd
        autoload - U add-zsh-hook
        add-zsh-hook -Uz chpwd (){ 
          if [[ "$(pwd)" != "$HOME" ]]; then lsd; fi 
        }

        # some env vars that we can't set with nix
        UID=$(cat /etc/passwd | grep "${config.home.username}" | awk -F ':' '{printf $3}')
        which gpgconf > /dev/null && SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)" || true

        # key binds
        bindkey "^[[1~"   beginning-of-line
        bindkey "^[[4~"   end-of-line
        bindkey "^[[3~"   delete-char

        # mcfly
        eval "$(${pkgs.mcfly}/bin/mcfly init zsh)"

        # zoxide
        eval "$(${pkgs.zoxide}/bin/zoxide init zsh --cmd cd)"

        # nix-direnv
        #eval "$(${pkgs.direnv}/bin/direnv hook zsh)"
      '';

      history = {
        path = "$HOME/.cache/zsh_history";
      };

      sessionVariables = { };

      shellAliases = {
        ls = "lsd";
        la = "lsd -alg";
        mkdir = "mkdir -p";
        cat = "bat --paging=never --style=plain";

        wh = "wormhole-rs";
        tree = "erd";

        ns = "nix-shell --run ${pkgs.zsh}/bin/zsh";
        nsp = "ns -p";
        nd = "nix develop -c zsh";
      };

      oh-my-zsh = {
        enable = false;
        plugins = [ ];
      };
    };
  };
}
