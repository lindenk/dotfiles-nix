{ config, lib, ... }:
with lib;
let cfg = config.modules.alacritty;
in
{
  options.modules.alacritty = {
    enable = mkEnableOption "Alacritty terminal";
  };

  config = mkIf cfg.enable {
    programs.alacritty = {
      enable = true;
      settings = importTOML ./alacritty/alacritty.toml;
    };
  };
}